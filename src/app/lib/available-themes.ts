import { QuizTheme } from './enums/QuizTheme';
import { ITheme } from './interfaces/ITheme';

export const themes: Array<ITheme> = [
  {
    name: 'component.theme_switcher.themes.material.name',
    preview: 'component.theme_switcher.themes.material.preview',
    description: 'component.theme_switcher.themes.material.description',
    id: QuizTheme.Material,
  }, {
    name: 'component.theme_switcher.themes.theme-arsnova-dot-click-contrast.name',
    preview: 'component.theme_switcher.themes.theme-arsnova-dot-click-contrast.preview',
    description: 'component.theme_switcher.themes.theme-arsnova-dot-click-contrast.description',
    id: QuizTheme.ArsnovaDotClickContrast,
  }, {
    name: 'component.theme_switcher.themes.black_beauty.name',
    preview: 'component.theme_switcher.themes.black_beauty.preview',
    description: 'component.theme_switcher.themes.black_beauty.description',
    id: QuizTheme.Blackbeauty,
  }, {
    name: 'component.theme_switcher.themes.elegant.name',
    preview: 'component.theme_switcher.themes.elegant.preview',
    description: 'component.theme_switcher.themes.elegant.description',
    id: QuizTheme.Elegant,
  }, {
    name: 'component.theme_switcher.themes.decent_blue.name',
    preview: 'component.theme_switcher.themes.decent_blue.preview',
    description: 'component.theme_switcher.themes.decent_blue.description',
    id: QuizTheme.DecentBlue,
  }, {
    name: 'component.theme_switcher.themes.material_hope.name',
    preview: 'component.theme_switcher.themes.material_hope.preview',
    description: 'component.theme_switcher.themes.material_hope.description',
    id: QuizTheme.MaterialHope,
  }, {
    name: 'component.theme_switcher.themes.material-blue.name',
    preview: 'component.theme_switcher.themes.material-blue.preview',
    description: 'component.theme_switcher.themes.material-blue.description',
    id: QuizTheme.MaterialBlue,
  }, {
    name: 'component.theme_switcher.themes.spiritual-purple.name',
    preview: 'component.theme_switcher.themes.spiritual-purple.preview',
    description: 'component.theme_switcher.themes.spiritual-purple.description',
    id: QuizTheme.SpiritualPurple,
  }, {
    name: 'component.theme_switcher.themes.GreyBlue-Lime.name',
    preview: 'component.theme_switcher.themes.GreyBlue-Lime.preview',
    description: 'component.theme_switcher.themes.GreyBlue-Lime.description',
    id: QuizTheme.GreyBlueLime,
  }, {
    name: 'component.theme_switcher.themes.westermann-blue.name',
    preview: 'component.theme_switcher.themes.westermann-blue.preview',
    description: 'component.theme_switcher.themes.westermann-blue.description',
    id: QuizTheme.WestermannBlue,
  },
];
